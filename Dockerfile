#Dockerfile
FROM python:3.7

ENV PYTHONUNBUFFERED 1

RUN mkdir /timestamp

WORKDIR /timestamp

ADD . /timestamp/

RUN pip install -r requirements.txt