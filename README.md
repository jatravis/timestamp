# Timestamp
This timestamp application is a simple Django/Django Rest Framwork application that contains a single GET endpoint to return the current time (UTC timezone and ISO format)

# Getting Started
1. Clone the repository ```git clone [repo]```
2. Create and run the docker container ```docker-compose up```
3. Access the app at [localhost:8000](localhost:8000)
4. Access the end point by navigating to [localhost:8000/get-time](localhost:8000/get-time)

