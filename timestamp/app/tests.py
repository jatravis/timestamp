from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIRequestFactory
from timestamp.app.views import TimestampViewSet
from datetime import datetime, timezone


# Add more Tests
class TimestampTests(TestCase):

    # Add test case using APIRequestFactory
    def test_timestamp_get(self):
        factory = APIRequestFactory()
        view = TimestampViewSet.as_view({'get': 'list'})
        request = factory.get('/get-time/')
        response = view(request)

        # May need to change this test to not test generated timestamps
        # Could fail on edge case
        mockTime = datetime.now(timezone.utc).replace(microsecond=0).isoformat()
        mockData = {"timestamp: {}".format(mockTime)}

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, mockData)
