# from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.response import Response
from datetime import datetime, timezone


# Timestamp viewset to setup DRF router
class TimestampViewSet(viewsets.ViewSet):

    # list is viewset equivalent of a get request
    def list(self, request, format=None):

        data = datetime.now(timezone.utc).replace(microsecond=0).isoformat()
        return Response({"timestamp: {}".format(data)})
