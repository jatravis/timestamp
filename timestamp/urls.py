from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from timestamp.app import views

# register the get-time endpoint
router = routers.DefaultRouter()
router.register(r'get-time', views.TimestampViewSet, base_name='get-time')

# include DRF router in url patterns
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
]
